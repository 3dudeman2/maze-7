/*
Author: Frank Leyva
File Name: Maze6.cpp
Date: 04/28/17
Assignment Summary:
For this project we need to write a program that converts our Maze3 into
a graph that uses vertex and edge objects. The program needs to then solve the
maze using these objects.

*/
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

using namespace std;

class Vertex;

//Node class that manages the linked lists
class Node {
	char data = 'o';
	string room = ".........";
	Node* next;
public:
	int xCord, yCord;
	bool visited = false;

	char getData() {
		return data;
	}
	void setData(char x) {
		data = x;
	}
	string getRoom() {
		return room;
	}
	Node* getNext() {
		return next;
	}
	void setRoom(string x) {
		room = x;
	}
	void setNext(Node* x) {
		next = x;
	}

	bool notOpenTile() {
		return (data == 'w');
	}

	~Node() {};
};

//Maze class that deals with all the maze stuff
class Maze {
	int height;
	int width;
	Node* head;
	Node* start;
	Node* finish;
public:
	int getHeight() {
		return height;
	}
	void setHeight(int newHeight) {
		height = newHeight;
	}
	void setHead(Node* newHead) {
		head = newHead;
	}
	void setWidth(int newWidth) {
		width = newWidth;
	}
	int getWidth() {
		return width;
	}

	Node* getHead() {
		return head;
	}
	void setStart(Node* s) {
		start = s;
	}
	void setFinish(Node* f) {
		finish = f;
	}
	Node* getStart() {
		return start;
	}
	Node* getFinish() {
		return finish;
	}

	bool isValidCoord(int x, int y) {
		bool valid = true;
		if (x >= width || x < 0)
			valid = false;
		if (y >= height || y < 0)
			valid = false;

		return valid;
	}

	//Gives all the tiles their coords. when they are created
	void setCoords() {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				Node* tmp = getPosition(i, j);
				if (tmp == NULL) {
					cout << "THE NODE AT " << j << ", "
						<< i << " IS NULL" << endl;
				}
				tmp->xCord = j;
				tmp->yCord = i;
			}
		}
	}

	//takes in coords. and returns correct node
	Node* getPosition(int x, int y) {
		bool valid = isValidCoord(x, y);
		if (valid) {
			int pos = x + (y*width);
			Node* tmp = head;
			for (int i = 0; i < pos; i++) {
				tmp = tmp->getNext();
			}
			return tmp;
		}
		else {
			////////cout << "returning null!" << endl;
			return NULL;
		}
	}

	Node* getPosition2(int x, int y) {
		bool valid = isValidCoord(x, y);
		if (valid) {
			int pos = x + (y*width);
			Node* tmp = head;
			for (int i = 0; i < pos; i++) {
				tmp = tmp->getNext();
			}
			if (tmp->getData() == 'w' || tmp->visited == true)
				return NULL;
			tmp->visited = true;
			return tmp;
		}
		else {
			////////cout << "returning null!" << endl;
			return NULL;
		}
	}

	void updateAt(int x, int y, string z) {
		Node* tmp = getPosition(y, x);
		tmp->setRoom(z);
	}

	void printMaze(ostream& ofs) {
		string spot, start;
		int counter = 0;
		start = "  ";
		for (int i = 0; i < width; i++) {
			start += " ";
			start += to_string(i);
			start += " ";
		}
		ofs << start << endl;
		ofs << "  ";
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < width; k++) {
					//here I used substr() to take parts of the 9 char string

					if (j == 0) {
						spot = this->getPosition(k, i)->getRoom().substr(0, 3);
					}
					if (j == 1) {
						spot = this->getPosition(k, i)->getRoom().substr(3, 3);
					}
					if (j == 2) {
						spot = this->getPosition(k, i)->getRoom().substr(6, 3);
					}
					ofs << spot;
					if (k == width - 1) {

						ofs << endl;
						if (counter % 3 == 0) {
							if (i > 9) {
								ofs << i;
							}
							else
								ofs << i << " ";
						}
						else
							ofs << "  ";
						counter++;
					}
				}
			}
		}
		ofs << endl;
	}

	bool notGoodTile(int x, int y) {
		Node* tmp = getPosition(x, y);
		return (NULL || tmp->getData() == 'w');
	}

	vector<Node*> getNeighbors(Node* node) {
		Node *North, *South, *East, *West;
		North = getPosition2(node->yCord, node->xCord - 1);
		South = getPosition2(node->yCord, node->xCord + 1);
		East = getPosition2(node->yCord + 1, node->xCord);
		West = getPosition2(node->yCord - 1, node->xCord);

		vector<Node*> neighbors; //N S E W
		neighbors.push_back(North);
		neighbors.push_back(South);
		neighbors.push_back(East);
		neighbors.push_back(West);
		return neighbors;
	}

	void unvisitTiles() {
		Node* tmp = start;
		while (tmp != NULL) {
			tmp->visited = false;
			tmp = tmp->getNext();
		}
	}

	~Maze() {}
};

class Edge {
public:
	Vertex *vert1, *vert2;
	int dist;
	bool visited;
	Edge(Vertex* start, Vertex* finish, int num) {
		vert1 = start;
		vert2 = finish;
		dist = num;
	}
	Vertex* goTo(Vertex* from) {
		return from == vert1 ? vert2 : vert1;
	}
};

class Vertex {
public:
	bool visited = false;
	int Num = 0;
	Edge *northE, *southE, *eastE, *westE;
	Vertex *north, *south, *east, *west;
	int yCord, xCord;
	string room;

	Vertex(Node* spot) {
		xCord = spot->yCord;
		yCord = spot->xCord;

	}
	vector<Vertex*> getTheNeighborEdges() {
		vector<Vertex*> theNeighbors; //N S E W
		if (northE != NULL && northE->goTo(this)->visited == false)
			theNeighbors.push_back(northE->goTo(this));

		if (southE != NULL && southE->goTo(this)->visited == false)
			theNeighbors.push_back(southE->goTo(this));

		if (eastE != NULL && eastE->goTo(this)->visited == false)
			theNeighbors.push_back(eastE->goTo(this));

		if (westE != NULL && westE->goTo(this)->visited == false)
			theNeighbors.push_back(westE->goTo(this));

		return theNeighbors;
	}

	vector<Vertex*> getTheNeighbor() {
		vector<Vertex*> theNeighbors; // N S E W
		//north check
		if (north != NULL && north->visited == false)
			theNeighbors.push_back(north);
		else
			theNeighbors.push_back(NULL);
		//south check

		if (south != NULL && south->visited== false)
			theNeighbors.push_back(south);
		else
			theNeighbors.push_back(NULL);

		//east check
		if (east != NULL && east->visited == false)
			theNeighbors.push_back(east);
		else
			theNeighbors.push_back(NULL);

		//west check
		if (west != NULL && west->visited == false)
			theNeighbors.push_back(west);
		else
			theNeighbors.push_back(NULL);

		return theNeighbors;
	}
};

class Queue {
	vector<Vertex*> que;
public:

	//returns the element we just removed 
	Vertex* pop_back() {
		Vertex* tmp = que.front();
		que.erase(que.begin(), que.begin() + 1);
		return tmp;
	}

	void push_back(Vertex* x) {
		que.push_back(x);
	}
	void print() {
		for (size_t i = 0; i < que.size(); i++) {
			cout << que[i]->yCord << ", " << que[i]->xCord << endl;
		}
	}
	bool empty() {
		bool empty = que.empty();
		return empty;
	}
	int size() {
		return que.size();
	}
	Vertex* get(int x) {
		return que[x];
	}

};

class Graph {
public:
	Maze* maze;
	Vertex *start, *finish;
	vector<Vertex*> vertices, path;
	int width, height;

	Graph(Maze* solvedMaze) {
		maze = solvedMaze;
		solvedMaze->unvisitTiles();
		start = new Vertex(solvedMaze->getStart());
		finish = new Vertex(solvedMaze->getFinish());
		width = solvedMaze->getWidth();
		height = solvedMaze->getHeight();
		setUpVertices(maze->getStart());

	}


	//pass in the start node
	Vertex* setUpVertices(Node* tmp) {
		//get its neighbors
		if (tmp == maze->getStart()) {
			vector<Node*> neighbors = maze->getNeighbors(tmp);
			start = new Vertex(tmp);
			start->room = tmp->getRoom();
			start->north = setUpVertices(neighbors[0]);
			start->south = setUpVertices(neighbors[1]);
			start->east = setUpVertices(neighbors[2]);
			start->west = setUpVertices(neighbors[3]);
			vertices.push_back(start);
			return NULL;
		}
		else if (tmp != NULL) {
			vector<Node*> neighbors = maze->getNeighbors(tmp);
			Vertex *vert = new Vertex(tmp);
			vert->room = tmp->getRoom();
			vert->north = setUpVertices(neighbors[0]);
			vert->south = setUpVertices(neighbors[1]);
			vert->east = setUpVertices(neighbors[2]);
			vert->west = setUpVertices(neighbors[3]);
			vertices.push_back(vert);
			return vert;
		}
		return NULL;
		/*IDEA for limiting verteices
		based on how many neighbors are valid...
		if 0 = create vertex and return it
		if 1 = keep moving in that direction (pass node to this function)
		if 2+ = create vertex and return it*/
	}

	bool checkForZero() {
		bool zero = false;
		for (int i = 0; i < vertices.size(); ++i) {
			if (vertices[i]->Num == 0) {
				zero = true;
				break;
			}
		}
		return zero;
	}

	vector<Vertex*>solveGraph(Vertex* vert, vector<Vertex*> pathSoFar, int j) {
		j++;
		pathSoFar.push_back(vert);
		if (vert->xCord != finish->xCord || vert->yCord != finish->yCord) {
			vector<Vertex*> neighbors = vert->getTheNeighborEdges();
			cout << "Turn " << j << " = ";
			cout << "vert " << vert->Num << " at "
				<< vert->xCord << ", " << vert->yCord;
			cout << " has " << neighbors.size() << " new edges" << endl;
			for (int i = 0; i < neighbors.size(); ++i) {
				if (pathSoFar.back()->xCord == finish->xCord
					&& pathSoFar.back()->yCord == finish->yCord) {
					break;
				}
				vert->visited = true;
				pathSoFar = solveGraph(neighbors[i], pathSoFar, j);

			}
			return pathSoFar;
		}
		else {
			cout << "Finish at " << vert->xCord << ", " << vert->yCord << endl;
			return pathSoFar;
		}

	}

	void showPathToFinish(vector<Vertex*> path) {
		cout << endl << endl;
		cout << "PATH TO FINISH" << endl;
		cout << "vvv START vvv" << endl;
		for (int i = 0; i < path.size(); ++i) {
			cout << "Vertex Number: " << path[i]->Num;
			cout << " Coords: " << path[i]->xCord
				<< ", " << path[i]->yCord << endl;
		}
		cout << "^^^ FINISH ^^^" << endl << endl;
	}

	bool isValidCoord(int x, int y) {
		bool valid = true;
		if (x >= width || x < 0)
			valid = false;
		if (y >= height || y < 0)
			valid = false;
		return valid;
	};

	//returns vert or NULL if wall or OoR
	Vertex* getVertAt(int x, int y) {
		bool valid = isValidCoord(x, y);
		if (valid) {
			Vertex* tmp = NULL;
			for (int i = 0; i < vertices.size(); ++i) {
				if (vertices[i]->xCord == x && vertices[i]->yCord == y) {
					tmp = vertices[i];
				}
			}
			return tmp;
		}
		else {
			return NULL;
		}
	}

	//v Extra Functionality v//
	void showEdges() {

		for (int i = 0; i < vertices.size(); ++i) {
			if (vertices[i]->northE != NULL) {//north
				vertices[i]->room.replace(1, 1, "^");
			}
			if (vertices[i]->southE != NULL) {//south
				vertices[i]->room.replace(7, 1, "v");
			}
			if (vertices[i]->eastE != NULL) {//east
				vertices[i]->room.replace(5, 1, ">");
			}
			if (vertices[i]->westE != NULL) {//west
				vertices[i]->room.replace(3, 1, "<");
			}
			if (vertices[i] != start && vertices[i] != finish)
				vertices[i]->room.replace(4, 1, "+");

		}
	}
	//^ Extra Functionality ^//

	void printGraph(ostream& ofs) {
		showEdges();
		string spot, start;
		int counter = 0;
		start = "  ";
		for (int i = 0; i < width; i++) {
			start += " ";
			start += to_string(i);
			start += " ";
		}
		ofs << start << endl;
		ofs << "  ";
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < 3; j++) {
				for (int k = 0; k < width; k++) {
					//here I used substr() to take parts of the 9 char string
					spot = "XXX";
					if (j == 0) {
						if (getVertAt(k, i) != NULL)
							spot = getVertAt(k, i)->room.substr(0, 3);
					}
					if (j == 1) {
						if (getVertAt(k, i) != NULL)
							spot = getVertAt(k, i)->room.substr(3, 3);
					}
					if (j == 2) {
						if (getVertAt(k, i) != NULL)
							spot = getVertAt(k, i)->room.substr(6, 3);
					}
					ofs << spot;
					if (k == width - 1) {

						ofs << endl;
						if (counter % 3 == 0) {
							if (i > 9) {
								ofs << i;
							}
							else
								ofs << i << " ";
						}
						else
							ofs << "  ";
						counter++;
					}
				}
			}
		}
		ofs << endl;
	}

	Vertex* getZeroVert() {
		Vertex* tmp=NULL;
		for (int i = 0; i < vertices.size(); ++i) {
			if (vertices[i]->Num == 0) {
				tmp = vertices[i];
				break;
			}
		}
		return tmp;
	}

};

//Algorithm from the book
void breathFirstSearch(Graph graph) {
	int i = 1;
	Queue path;
	Vertex* start = graph.start;
	while (graph.checkForZero()) {
		Vertex* zeroVert = graph.getZeroVert();
		zeroVert->Num = i++;
		path.push_back(zeroVert);
		while (!path.empty()) {
			Vertex* vert = path.pop_back();
			vector<Vertex*> neighbors = vert->getTheNeighbor();
			if (neighbors[0]!= NULL && neighbors[0]->Num == 0) {
				neighbors[0]->Num = i++;
				path.push_back(neighbors[0]);
				//attach North edge
				Edge *tmp = new Edge(vert, vert->north, 1);
				vert->northE = tmp;
				vert->north->southE = tmp;
			}
			if (neighbors[1] != NULL && neighbors[1]->Num == 0) {
				neighbors[1]->Num = i++;
				path.push_back(neighbors[1]);
				//attach South edge
				Edge *tmp = new Edge(vert, vert->south, 1);
				vert->southE = tmp;
				vert->south->northE = tmp;
			}
			if (neighbors[2] != NULL && neighbors[2]->Num == 0) {
				neighbors[2]->Num = i++;
				path.push_back(neighbors[2]);
				//attach East edge
				Edge *tmp = new Edge(vert, vert->east, 1);
				vert->eastE = tmp;
				vert->east->westE = tmp;
			}
			if (neighbors[3] != NULL && neighbors[3]->Num == 0) {
				neighbors[3]->Num = i++;
				path.push_back(neighbors[3]);
				//attach West edge
				Edge *tmp = new Edge(vert, vert->west, 1);
				vert->westE = tmp;
				vert->west->eastE = tmp;
			}
		}
	}
};

//Finds the finish and prints it all out
void findFinish(Maze* maze) {
	ofstream saveFile("output_file.txt");
	Graph myGraph(maze);
	breathFirstSearch(myGraph);
	int steps = 0;
	vector<Vertex*> finished =
		myGraph.solveGraph(myGraph.start, myGraph.path, steps);

	myGraph.showPathToFinish(finished);
	myGraph.printGraph(cout);
	myGraph.printGraph(saveFile);
}

//Seperate function for just setting up the Maze Object
Maze* setUpMaze() {
	Maze* newMaze = new Maze();
	ifstream file1;
	stringstream data;
	file1.open("input_file.txt");
	if (!file1.is_open())
		cout << "input_file.txt could not be opened" << endl;
	string line, xSize, ySize;
	getline(file1, line, ',');
	data.str(line);
	data >> xSize;
	data.clear();
	getline(file1, line, '.');
	data.str(line);
	data >> ySize;
	data.clear();
	Node* head = new Node();
	Node* tmp = head;
	for (int i = 0; i < stoi(xSize) * stoi(ySize); i++) {
		tmp->setNext(new Node());
		tmp = tmp->getNext();
	}
	tmp->setNext(NULL);
	newMaze->setHead(head);
	newMaze->setHeight(stoi(ySize));
	newMaze->setWidth(stoi(xSize));
	newMaze->setCoords();
	return newMaze;
}



//This function sets up all the blocks within the maze array
//its very long, and in the future projects I will work towards
//splitting it up a bit
Maze* readFromFile() {
	ifstream file1;
	stringstream data;
	file1.open("input_file.txt");
	if (!file1.is_open())
		cout << "input_file.txt could not be opened" << endl;
	string line, yStart, xStart, xFinish, yFinish;
	Maze* newMaze = setUpMaze();
	//Using string splicing to get the correct coords. for the Start Block
	getline(file1, line, '(');
	data.str(line);
	data.clear();
	getline(file1, line, ',');
	data.str(line);
	data >> xStart;
	data.clear();
	getline(file1, line, '.');
	data.str(line);
	data >> yStart;
	data.clear();
	newMaze->getPosition(stoi(xStart), stoi(yStart))->setRoom("....S....");
	newMaze->getPosition(stoi(xStart), stoi(yStart))->setData('s');
	newMaze->setStart(newMaze->getPosition(stoi(xStart), stoi(yStart)));

	//Using strings splicing to get the correct coords. for the Finish Block
	getline(file1, line, '(');
	data.str(line);
	data.clear();
	getline(file1, line, ',');
	data.str(line);
	data >> xFinish;
	data.clear();
	getline(file1, line, '.');
	data.str(line);
	data >> yFinish;
	newMaze->getPosition(stoi(xFinish), stoi(yFinish))->setRoom("....F....");
	newMaze->getPosition(stoi(xFinish), stoi(yFinish))->setData('f');
	newMaze->setFinish(newMaze->getPosition(stoi(xFinish), stoi(yFinish)));
	while (true) {//Fills all the correct spots with Walls
		int x, y;
		if (file1.eof())
			break;
		getline(file1, line, '(');
		data.str(line);
		data.clear();
		getline(file1, line, ',');
		data.str(line);
		data >> x;
		data.clear();
		getline(file1, line, ')');
		data.str(line);
		data >> y;
		data.clear();
		newMaze->getPosition(x, y)->setRoom("XXXXXXXXX");
		newMaze->getPosition(x, y)->setData('w');
	}
	return newMaze;
}

//everything just goes through the openFile() function
int main() {
	Maze* maze = readFromFile();
	findFinish(maze);
	return 0;
}